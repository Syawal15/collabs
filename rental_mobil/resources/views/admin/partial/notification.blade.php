<script>
    @if (session('notif'))
        $.notify({
            message: '{{session('notif')}}' 
        },{
            
            type: 'success'
        });
    @endif
</script>