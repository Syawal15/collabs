<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">RENTAL MOBIL</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">RM</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Menu</li>
        <li class="nav-item dropdown {{request()->routeIs('admin.dashboard.index') ? 'active' : ''}}">
          <a href="{{route('admin.dashboard.index')}}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('admin.merk*') ? 'active' : ''}}">
          <a href="{{route('admin.merk.index')}}" class="nav-link"><i class="fas fa-clipboard-list"></i><span>Merk Mobil</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('admin.mobil*') ? 'active' : ''}}">
          <a href="{{route('admin.mobil.index')}}" class="nav-link"><i class="fas fa-car"></i><span>Daftar Mobil</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('admin.transaksi*') ? 'active' : ''}}">
          <a href="{{route('admin.transaksi.index')}}" class="nav-link"><i class="fas fa-money-check"></i><span>Daftar Transaksi</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('admin.user*') ? 'active' : ''}}">
          <a href="{{route('admin.user.index')}}" class="nav-link"><i class="fas fa-users"></i><span>Daftar User</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('admin.profile*') ? 'active' : ''}}">
          <a href="{{route('admin.profile.index')}}" class="nav-link"><i class="fas fa-user"></i><span>Profil</span></a>
        </li>
    </ul>
</aside>