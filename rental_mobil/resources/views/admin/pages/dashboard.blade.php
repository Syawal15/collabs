@extends('admin.layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
            <i class="fas fa-car"></i>
            </div>
            <div class="card-wrap">
            <div class="card-header">
                <h4>Jumlah Mobil</h4>
            </div>
            <div class="card-body">
                {{$t_cars}}
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
            <i class="fas fa-money-check"></i>
            </div>
            <div class="card-wrap">
            <div class="card-header">
                <h4>Jumlah Transaksi</h4>
            </div>
            <div class="card-body">
                {{$t_transaksi}}
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
            <i class="fas fa-receipt"></i>
            </div>
            <div class="card-wrap">
            <div class="card-header">
                <h4>Pembayaran</h4>
            </div>
            <div class="card-body">
                {{$t_pembayaran}}
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
            <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
            <div class="card-header">
                <h4>Jumlah User</h4>
            </div>
            <div class="card-body">
                {{$t_users}}
            </div>
            </div>
        </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
          <h4>Desain ERD Aplikasi Rental Mobil</h4>
        </div>
        <div class="card-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>
@endsection
