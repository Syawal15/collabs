@extends('admin.layouts.master')

@section('title')
    Detail Pembayaran
@endsection

@section('content')
    <div class="card">
        <table class="table">
            <tr>
                @foreach ($pembayaran as $pmb)
                <td class="bold">ID</td><td>{{$pmb->id}}</td>
            </tr>
            <tr>
                <td>Bukti Pembayaran</td>
                <td>
                    <a href="{{route('admin.pembayaran.download',$pmb->id)}}" class="btn btn-sm btn-info"">
                        <i class="fas fa-download"> Download Bukti Pembayaran</i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Status Pembayaran</td>
                <td>
                    @if ($pmb->status_pembayaran == 0)
                    <span class="badge badge-danger">Menunggu Pembayaran</span>    
                    @elseif ($pmb->status_pembayaran == 1)
                    <span class="badge badge-info">Menunggu Konfirmasi</span>
                    @elseif ($pmb->status_pembayaran == 2)
                    <span class="badge badge-success">Terkonfirmasi</span>
                    @endif    
                </td>
            </tr>        
        </table>
        <form class="mr-4 ml-4 mb-4 mt-0"action="{{route('admin.pembayaran.update',$pmb->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <input type="text" class="form-control" name="status_pembayaran" value="2" id="status_pembayaran" hidden>
            </div>
            <button type="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>
        </form>
        @endforeach
    </div>
@endsection
