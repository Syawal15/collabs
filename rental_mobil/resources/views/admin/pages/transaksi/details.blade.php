@extends('admin.layouts.master')

@section('title')
    Detail Transaksi
@endsection

@section('content')
    <div class="card">
        <table class="table">
            <tr>
                <td class="bold">ID</td><td>{{$transaksi->id}}</td>
            </tr>
            <tr>
                <td>Tanggal Rental</td><td>{{$transaksi->tanggal_rental}}</td>
            </tr>
            <tr>
                <td>Tanggal Kembali</td><td>{{$transaksi->tanggal_kembali}}</td>
            </tr>
            <tr>
                <td>Durasi Rental</td><td>{{$transaksi->durasi_rental}} hari</td>
            </tr>
            <tr>
                <td>Harga Per Hari</td><td>Rp. {{$transaksi->harga}}</td>
            </tr>
            <tr>
                <td>Denda Per Hari</td><td>Rp. {{$transaksi->denda}}</td>
            </tr>
            <tr>
                <td>Total Pembayaran</td><td>Rp. {{$transaksi->total_pembayaran}}</td>
            </tr>
            <tr>
                <td>Tanggal Pengembalian</td><td>{{$transaksi->tanggal_pengembalian}}</td>
            </tr>
            <tr>
                <td>Total Denda</td><td>Rp. {{$transaksi->total_denda}}</td>
            </tr>
            <tr>
                <td>Status Pengembalian</td><td>
                    @if ($transaksi->status_pengembalian == 0)
                        <span class="badge badge-danger">Belum Kembali</span>
                    @elseif ($transaksi->status_pengembalian == 1)
                        <span class="badge badge-success">Sudah Kembali</span>
                    @endif
                </td>
            </tr>
            <tr>
                <td>Status Transaksi</td><td>
                    @if ($transaksi->status_transaksi == 0)
                        <span class="badge badge-danger">Dibatalkan</span>
                    @elseif ($transaksi->status_transaksi == 1)
                        <span class="badge badge-info">Berlangsung</span>
                    @elseif ($transaksi->status_transaksi == 2)
                        <span class="badge badge-success">Selesai</span>
                    @endif
                </td>
            </tr>
            <tr>
                <td>Users Id</td><td>{{$transaksi->users_id}}</td>
            </tr>
            <tr>
                <td>Mobil Id</td><td>{{$transaksi->mobil_id}}</td>
            </tr>        
        </table>
    </div>
@endsection
