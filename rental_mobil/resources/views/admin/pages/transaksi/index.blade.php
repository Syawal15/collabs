@extends('admin.layouts.master')

@section('title')
    Daftar Transaksi
@endsection

@section('content')
    <div class="card">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Id Transaksi</th>
                <th scope="col">Status Pembayaran</th>
                <th scope="col">Status Pengembalian</th>
                <th scope="col">Status Transaksi</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($transaksi as $tr)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$tr->id}}</td>
                        <td>
                        @foreach ($tr->pembayaran as $item)
                            @if ($item->status_pembayaran == 0)
                            <span class="badge badge-danger">Menunggu Pembayaran</span>    
                            @elseif ($item->status_pembayaran == 1)
                            <span class="badge badge-info">Menunggu Konfirmasi</span>
                            @elseif ($item->status_pembayaran == 2)
                            <span class="badge badge-success">Terkonfirmasi</span>
                            @endif    
                        @endforeach
                        </td>
                        <td>
                            @if ($tr->status_pengembalian == 0)
                            <span class="badge badge-danger">Belum Kembali</span>
                            @elseif ($tr->status_pengembalian == 1)
                                <span class="badge badge-success">Sudah Kembali</span>
                            @endif
                        </td>
                        <td>
                            @if ($tr->status_transaksi == 0)
                                <span class="badge badge-danger">Dibatalkan</span>
                            @elseif ($tr->status_transaksi == 1)
                                <span class="badge badge-info">Berlangsung</span>
                            @elseif ($tr->status_transaksi == 2)
                                <span class="badge badge-success">Selesai</span>
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin.transaksi.destroy', $tr->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="{{route('admin.transaksi.aksi', $tr->id)}}" title="Selesaikan Transaksi" class="btn btn-success">
                                    <i class="fas fa-check"></i>
                                </a>
                                <a href="{{route('admin.transaksi.show', $tr->id)}}" title="Lihat Detail Transaksi" class="btn btn-info">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <a href="{{route('admin.pembayaran.show', $tr->id)}}" class="btn btn-primary" title="Cek Pembayaran">
                                    <i class="fas fa-edit"></i>
                                </a>
                                {{-- <a href="/cast/{{$tr->id}}/delete" class="btn btn-danger" title="Hapus Transaksi">
                                    <i class="fas fa-trash"></i>
                                </a> --}}
                                <button type="submit" class="btn btn-danger" value="">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="5">
                        <td class>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection
