@extends('admin.layouts.master')

@section('title')
    Selesaikan Transaksi
@endsection

@section('content')
    <div class="card">
        <form class="mr-4 ml-4 mb-4" action="{{route('admin.transaksi.update', $transaksi->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <input type="text" class="form-control" name="status_pengembalian" value="1" id="status_pengembalian" hidden>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="status_transaksi" value="2" id="status_transaksi" hidden>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="tanggal_kembali" value="{{$transaksi->tanggal_kembali}}" id="tanggal_kembali" hidden>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="denda" value="{{$transaksi->denda}}" id="denda" hidden>
            </div>
            <div class="form-group">
                <label for="tanggal_pengembalian">Tanggal Pengembalian</label>
                <input type="date" class="form-control" name="tanggal_pengembalian" id="tanggal_pengembalian" placeholder="Masukkan Tanggal Pengembalian">
                @error('tanggal_pengembalian')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Selesai</button>
        </form>
    </div>
@endsection