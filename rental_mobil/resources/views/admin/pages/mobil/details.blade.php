@extends('admin.layouts.master')

@section('title')
    Detail Mobil
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
        </div>
        <div class="card">
            <div class="card-body">
                <div class=" ">
                    <div data-crop-image="285" style="overflow: hidden; position: relative; height: 285px;">
                        <img class="d-block img-fluid" height="500px" width="500px"
                            src="{{ asset('uploads/'. $mobil->gambar) }} ">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <h3>{{$mobil->nama}}( {{$mobil->no_plat}} )</h3>
            <p>Mobil Tahun {{$mobil->tahun}} berwarna {{$mobil->warna}} Masih {{$mobil->status}} <br>
            Disewakan dengan harga Rp.{{$mobil->harga}} saja perharinya <br>
            Langsung Hubungi Contact Yang tertera
            </p>
            
        </div>
    </div>
@endsection
