@extends('admin.layouts.master')

@section('title')
    Daftar Mobil
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{ route('admin.mobil.create') }}" class="btn btn-primary">Tambah Daftar Mobil</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table-mobil" class="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Merk</th>
                        <th>No Plat</th>
                        <th>Harga</th>
                        <th>Gambar</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form action="" method="POST" id="deleteForm" style="display: none;">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('addon-script')
@include('admin.partial.notification')
<script>
    $(function(){
        $('#table-mobil').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.mobil.index') }}",
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'nama', name: 'nama'},
                {data: 'merk.nama', name: 'merk.nama'},
                {data: 'no_plat', name: 'no_plat'},
                {data: 'harga', name: 'harga'},
                {data: 'gambar', name: 'gambar'},
                {data: 'action', name: 'action'}
            ],
        });
    })
</script>
@endpush