<a href="{{route('admin.mobil.show', $id)}}" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i></a>
<a href="{{route('admin.mobil.edit', $id)}}" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
<a href="{{route('admin.mobil.destroy', $id)}}" class="btn btn-sm btn-danger" id="delete-{{$id}}"><i class="fas fa-trash"></i></a>
<script>
    $('a#delete-{{$id}}').on('click', function(e){
        e.preventDefault()
        let href = $(this).attr('href')
        Swal.fire({
            title: 'Apakah anda ingin menghapus?',
            text: 'Data akan dihapus permanen',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                document.getElementById('deleteForm').action = href
                document.getElementById('deleteForm').submit()
            }
            })
    })
</script>