@extends('admin.layouts.master')

@section('title')
    Daftar Merk Mobil
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.merk.index') }}" class="btn btn-primary" data-toggle="modal"
                data-target="#modalmerk">Tambah Merk Mobil</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-md">
                    <tbody>
                        @if ($merk->isNotEmpty())
                            
                        <tr>
                            <th>No. </th>
                            <th>Nama</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($merk as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->nama }}</td>
                                    <td><a class="btn"><form action="/admin/merk/{{$value->id}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="Hapus" class="btn btn-danger">
                                      </form></a></td>
                                </tr>

                            @endforeach
                        @else
                            <tr colspan="3">
                                <td>No data</td>
                            </tr>
                    </tbody>
                    @endif

                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            <nav class="d-inline-block">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1 <span
                                class="sr-only">(current)</span></a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
