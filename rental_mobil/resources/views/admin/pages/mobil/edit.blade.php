@extends('admin.layouts.master')

@section('title')
    Form Edit Mobil
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Masukkan Data-Data Yang Diubah</h4>
        </div>
        <div class="card-body">
            <form method="POST" action="/admin/mobil/{{$mobil->id}}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Nama mobil</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{old('nama') ?? $mobil->nama}}">
                    @error('nama')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Masukkan Merk Mobil</label>
                    <select class="form-control @error('merk_mobil_id') is-invalid @enderror" name="merk_mobil_id" id="merk_mobil_id">
                        @foreach ($merk as $value) { 
                            <option value="{{ $value->id }}">{{ $value->nama }}</option>
                        }
                        @endforeach 
                    </select>
                    @error('merk_mobil')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <button class="btn btn-primary mt-3" data-toggle="modal" data-target="#modalmobil" type="button">Tambah Merk Mobil</button>

                </div>
                <div class="form-group">
                    <label>Masukkan Plat Nomor Mobil</label>
                    <input type="text" class="form-control @error('no_plat') is-invalid @enderror" name="no_plat" id="no_plat" value="{{ old('no_plat') ?? $mobil->no_plat}}">
                    @error('no_plat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Masukkan Warna Mobil</label>
                    <input type="text" class="form-control @error('warna') is-invalid @enderror" name="warna" id="warna" value="{{ old('warna') ?? $mobil->warna}}">
                    @error('warna')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Masukkan Tahun Mobil</label>
                    <input type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" id="tahun" value="{{ old('tahun') ?? $mobil->tahun}}">
                    @error('tahun')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Masukkan Harga Mobil</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                Rp
                            </div>
                        </div>
                        <input type="text" class="form-control @error('harga') is-invalid @enderror" name="harga" id="harga" value="{{ old('harga') ?? $mobil->harga}}">
                        @error('harga')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label>Masukkan Denda</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                Rp
                            </div>
                        </div>
                        <input type="text" class="form-control @error('denda') is-invalid @enderror" name="denda" id="denda" value="{{ old('denda') ?? $mobil->denda}}">
                        @error('denda')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label>Masukkan Gambar Mobil</label>
                    <br>
                    <img src="{{asset('uploads/'. $mobil->gambar)}}" class="mb-3" style="width:100px;" alt="">
                    <input class="form-control" type="file" name="gambar" id="denda">
                    @error('gambar')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Status Mobil</label>
                    <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
                        <option value="0">Tersedia</option>
                        <option value="1">Tidak Tersedia</option>
                    </select>
                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group mb-0">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
