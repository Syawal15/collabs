@extends('admin.layouts.master')

@section('title')
    Detail User
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name" type="text" class="form-control" value="{{$user->name}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input id="email" type="email" class="form-control" value="{{$user->email}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Address</label>
                        <textarea id="alamat" class="form-control" rows="3" readonly>{{$user->profile->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="no_telepon">No telepon</label>
                        <input id="no_telepon" type="number" class="form-control" value="{{$user->profile->no_telepon}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="no_ktp">No KTP</label>
                        <input id="no_ktp" type="number" class="form-control" value="{{$user->profile->no_ktp}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <input id="role" type="text" class="form-control" value="{{$user->role}}" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
