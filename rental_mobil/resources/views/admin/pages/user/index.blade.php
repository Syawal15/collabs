@extends('admin.layouts.master')

@section('title')
    Daftar User
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{route('admin.user.create')}}" class="btn btn-primary">Buat user</a>
        <a href="{{route('admin.user.create')}}" class="btn btn-danger ml-2">Hapus semua user</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table-user" class="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form action="" method="POST" id="deleteForm" style="display: none;">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('addon-script')
@include('admin.partial.notification')
<script>
    $(function(){
        $('#table-user').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.user.index') }}",
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'role', name: 'role'},
                {data: 'action', name: 'action'}
            ],
        });
    })
</script>
@endpush
    