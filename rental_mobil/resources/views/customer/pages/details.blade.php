@extends('customer.layouts.master')

@section('title')
    Detail Mobil
@endsection

@section('content')
<!-- Banner Starts Here -->
<div class="container">
  <div style="height: 150px;"></div>

  <div class="card">
    <div class="card-body">
        @foreach ($mobil as $mb)
      <div class="row">
        <div class="col-md-6">
          <img width="500px;" src="{{asset('uploads')}}/{{$mb->gambar}}" alt="">
        </div>
        <div class="col-md-6">
          <table class="table">
            <tr>
              <th>Merek</th>
              <td>{{$mb->nama}}</td>
            </tr>
            <tr>
              <th>No. Plat</th>
              <td>{{$mb->no_plat}}</td>
            </tr>
            <tr>
              <th>Warna</th>
              <td>{{$mb->warna}}</td>
            </tr>
            <tr>
              <th>Tahun Mobil</th>
              <td>{{$mb->tahun}}</td>
            </tr>
            <tr>
              <th>Status</th>
              <td>
                @if ($mb->status == '0')
                    Tersedia
                @else
                    Tidak tersedia / sedang dirental
                @endif
              </td>
              
            </tr>
            <tr>
            <td></td>
              <td>
                  @if ($mb->status == "1")
                  <span class="btn btn-danger">Telah Dirental</span>    
                  @else
                      <a href="/rental/{{$mb->id}}" class="btn btn-success">Rental</a>
                  @endif
              </td>
            </tr>
          </table>
        </div>
      </div>
                  
      @endforeach

    </div>
  </div>

</div>
<!-- Banner Ends Here -->

<div style="height: 180px;"></div>
@endsection