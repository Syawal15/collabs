@extends('customer.layouts.master')

@section('title')
    Daftar Mobil
@endsection

@section('content')
<div style="height: 40px;"></div>

<section class="blog-posts grid-system">
  <div class="container">
    <div class="all-blog-posts">
      <div class="row">

        @foreach ($mobil as $mb)
        <div class="col-md-4 col-sm-6">
          <div class="blog-post">
            <div class="blog-thumb mb-1">
              <img width=100 height=200 src="{{asset('uploads/')}}/{{$mb->gambar}}" alt="">
            </div>
            
            <div class="down-content" padding=20px>
              <div class="row">
                <div class="col-lg-12">
                  <ul class="post-tags d-flex justify-content-center">
                    <a href="offers.html">
                            <h4>{{$mb->nama}}</h4></a>    
                  </ul>
                  <ul class="post-tags d-flex justify-content-center">
                    <span>Rp. <?= number_format($mb->harga,0,',','.'); ?>,-</span> <strong>per hari</strong>
                  </ul>
                </div>
              </div>

              <div class="post-options">
                <div class="row">
                  <div class="col-lg-12">
                    <ul class="post-tags d-flex justify-content-center">
                        @if ($mb->status == "0")
                            <a class="btn btn-lg btn-success mt-2 mr-1" href="/rental/{{$mb->id}}"> Rental</a>    
                        @else
                            <a class="btn btn-lg btn-danger mt-2 mr-1" href="#"> Tidak Tersedia</a>    
                        @endif
                        <a class="btn btn-lg btn-info mt-2" href="/mobil/{{$mb->id}}"> Detail</a>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach

      </div>
    </div>
  </div>
</section>

<div style="height: 180px;"></div>

@endsection