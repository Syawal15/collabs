@extends('customer.layouts.master')

@section('title')
    Transaksi
@endsection

@section('content')
<div style="height: 150px;"></div>
<div class="container">
  <div class="card mx-auto">
    <div class="card-header">
      Data Transaksi Anda
    </div>
    <div class="card-body">
      <table class="table table-bordered table-striped">
        <tr>
          <th>No</th>
          <th>Merk Mobil</th>
          <th>No. Plat</th>
          <th>Total Pembayaran</th>
          <th>Action</th>
        </tr>

        <?php
        $no = 1;
        foreach($mobil as $mb): ?>
        <tr>
          <td><?= $no++; ?></td>
          <td>{{$mb->nama}}</td>
          <td><?= $mb->no_plat; ?></td>
          <td>Rp.<?= number_format($mb->total_pembayaran, 0, ',', '.'); ?>,-</td>
          <td>
            @if ($mb->status_transaksi == 2)
            <button class="btn btn-sm btn-danger">Rental Selesai</button>  
            @elseif ($mb->status_transaksi == 1)
            <a href="/pembayaran/{{$mb->id}}" class="btn btn-sm btn-success">Cek Pembayaran</a>
            @elseif ($mb->status_transaksi == 0)
            <a href="/checkout/{{$mb->id}}" class="btn btn-sm btn-info">Checkout</a>
            @endif
            @if ($mb->status_pengembalian == 0)
             <a onclick="return confirm('Yakin batal?')" class="btn btn-sm btn-danger" href="#">Batal</a>  
            @else
            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">   
              Batal
            </button>
            @endif
          </td>
        </tr>

        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>

<div style="height: 180px;"></div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informasi Batal Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Maaf, transaksi ini sudah selesai, dan tidak bisa dibatalkan!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
@endsection