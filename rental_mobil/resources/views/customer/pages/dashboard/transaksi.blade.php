@extends('customer.layouts.dashboard')

@section('title')
    Transaksi
@endsection

@section('content')
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
            <i class="fas fa-money-check"></i>
            </div>
            <div class="card-wrap">
            <div class="card-header">
                <h4>Jumlah Transaksi</h4>
            </div>
            <div class="card-body">
               
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
