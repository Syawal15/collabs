@extends('customer.layouts.dashboard')

@section('title')
    Profile
@endsection

@section('content')
<div class="card">
    <form method="post" action="{{route('customer.update')}}">
        @csrf
        @method('PUT')
        <div class="card-header">
            <h4>Edit Profile</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ??$user->name}}">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                </div>
                <div class="form-group col-md-6 col-12">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') ?? $user->email}}">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-7 col-12">
                    <label>No ktp</label>
                    <input type="number" name="no_ktp" class="form-control @error('no_ktp') is-invalid @enderror" value="{{old('no_ktp') ?? $user->profile->no_ktp}}">
                    @error('no_ktp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5 col-12">
                    <label>Phone</label>
                    <input type="number" name="no_telepon" class="form-control @error('no_telepon') is-invalid @enderror" value="{{old('no_telepon') ?? $user->profile->no_telepon}}">
                    @error('no_telepon')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    <label>Bio</label>
                    <textarea class="form-control" name="alamat" style="margin-top: 0px; margin-bottom: 0px; height: 166px;">{{$user->profile->alamat}}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                    <small class="form-text text-muted">Kosongkan jika tidak ingin mengganti pasword</small>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="password-confirm">Confirm password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-primary">Save Changes</button>
        </div>
    </form>
</div>
@endsection
@push('addon-script')
@include('admin.partial.notification')
@endpush