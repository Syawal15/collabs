<table style="width: 40%;">
    <h2>Invoice Pembayaran Anda</h2>
    <hr>
    <?php foreach($transaksi as $tr): ?>
    <tr>
        <td>ID Transaksi</td>
        <td>:</td>
        <td><?= $tr->id; ?></td>
      </tr>
    <tr>
        <td>Merek Mobil</td>
        <td>:</td>
        <td><?= $tr->nama; ?></td>
      </tr>
      <tr>
        <td>Tanggal Rental</td>
        <td>:</td>
        <td><?= date('d/m/Y', strtotime($tr->tanggal_rental)); ?></td>
      </tr>
      <tr>
        <td>Tanggal Kembali</td>
        <td>:</td>
        <td><?= date('d/m/Y', strtotime($tr->tanggal_kembali)); ?></td>
      </tr>
      <tr>
        <td>Biaya Sewa Perhari</td>
        <td>:</td>
        <td>Rp.<?= number_format($tr->harga, 0, ',', '.'); ?>,-</td>
      </tr>
      <tr>
        <td>Jumlah Hari Sewa</td>
        <td>:</td>
        <td><?= $tr->durasi_rental; ?> Hari</td>
      </tr>
  
      <tr>
        <td>Status Pembayaran</td>
        <td>:</td>
        <td>Lunas</td>
      </tr>
  
      <tr style="font-weight:bold;">
        <td>JUMLAH PEMBAYARAN</td>
        <td>:</td>
        <td>Rp.<?= number_format($tr->total_pembayaran, 0, ',', '.'); ?>,-</td>
      </tr>
      <?php endforeach; ?>
  </table>
  
  <script>
    window.print();
  </script>
  