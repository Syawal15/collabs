@extends('customer.layouts.master')

@section('title')
    Tambah Rental
@endsection

@section('content')
<div class="container">
    <div style="height: 150px;"></div>
    <div class="card">
      <card class="card-header">
        Form Rental Mobil
      </card> 
      <div class="card-body">
          @foreach ($mobil as $mb)
        <form action="/transaksi/{{$mb->id}}" method="post">
          @csrf
          <div class="form-group">
            <label for="">Harga Sewa/hari</label>
            <input type="hidden" name="users_id" value="{{$user}}">
            <input type="text" name="harga" class="form-control" value="{{$mb->harga}}" readonly>
          </div>
          <div class="form-group">
            <label for="">Denda/hari</label>
            <input type="text" name="denda" class="form-control" value="{{$mb->denda}}" readonly>
          </div>
          <div class="form-group">
            <label for="">Tanggal Rental</label>
            <input type="date" name="tanggal_rental" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Tanggal Kembali</label>
            <input type="date" name="tanggal_kembali" class="form-control">
          </div>
          <button type="submit" class="btn btn-primary">Rental</button>
        </form>
        @endforeach
      </div>
    </div>
  </div>
  
  <div style="height: 180px;"></div>  
@endsection