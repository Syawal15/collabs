<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{route('customer.index')}}">RENTAL MOBIL</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{route('customer.dashboard')}}">RM</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Menu</li>
        <li class="nav-item dropdown {{request()->routeIs('customer.dashboard') ? 'active' : ''}}">
          <a href="{{route('customer.dashboard')}}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('customer.transaksi*') ? 'active' : ''}}">
          <a href="{{route('customer.transaksi')}}" class="nav-link"><i class="fas fa-money-check"></i><span>Daftar Transaksi</span></a>
        </li>
        <li class="nav-item dropdown {{request()->routeIs('customer.profile*') ? 'active' : ''}}">
          <a href="{{route('customer.profile')}}" class="nav-link"><i class="fas fa-user"></i><span>Profil</span></a>
        </li>
    </ul>
</aside>