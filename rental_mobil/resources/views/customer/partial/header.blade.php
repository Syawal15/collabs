<!-- Header -->
<header class="">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="{{route('customer.index')}}"><h2>Aplikasi Rental Mobil</h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{route('customer.index')}}">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li> 
            <li class="nav-item">
              <a class="nav-link" href="{{route('customer.mobil')}}">Mobil</a>
            </li>
            @auth
            <li class="nav-item">
              <a class="nav-link" href="/daftar">Transaksi</a>
            </li>
            <li class="nav-item">
                @if (Auth::user()->role === 'admin')
                <a class="nav-link" href="{{route('admin.dashboard.index')}}">Dashboard</a>   
                @else    
                <a class="nav-link" href="{{route('customer.dashboard')}}">Dashboard</a>
                @endif
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Register</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
              </li>
            @endauth
          </ul>
        </div>
      </div>
    </nav>
</header>