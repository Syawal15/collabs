<!-- Bootstrap core CSS -->
<link href="{{asset('templates/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- Additional CSS Files -->
<link rel="stylesheet" href="{{asset('templates/assets/css/fontawesome.css')}}">
<link rel="stylesheet" href="{{asset('templates/assets/css/style_customer.css')}}">
<link rel="stylesheet" href="{{asset('templates/assets/css/owl.css')}}">