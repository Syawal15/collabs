<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
        <title>User Dashboard</title>
        @include('customer.partial.dashboard.style')
    </head>

    <body>
        <div id="app">
            <div class="main-wrapper">
                <div class="navbar-bg"></div>
                    @include('customer.partial.dashboard.navbar')
                <div class="main-sidebar">
                    @include('customer.partial.dashboard.sidebar')
                </div>

                <!-- Main Content -->
                <div class="main-content">
                    <section class="section">
                        <div class="section-header">
                            <h1>@yield('title')</h1>
                        </div>

                        <div class="section-body">
                            @yield('content')
                        </div>
                    </section>
                </div>
                @include('customer.partial.dashboard.footer')
            </div>
        </div>
    @include('customer.partial.dashboard.script')
    @stack('addon-script')
    </body>
</html>
