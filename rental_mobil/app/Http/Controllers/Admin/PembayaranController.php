<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pembayaran;

class PembayaranController extends Controller
{
    public function show($transaksi_id)
    {
        $pembayaran = DB::table('pembayaran')
                ->where('transaksi_id', '=', $transaksi_id)
                ->get();

        return view('admin.pages.pembayaran.details', compact('pembayaran'));
    }

    public function update($id, Request $request)
    {
        $pembayaran = Pembayaran::findOrfail($id);
        $input = $request->all();
        $pembayaran->update($input);
        return redirect('admin/transaksi');
    }

    public function download($id)
    {
        $pembayaran = Pembayaran::findOrfail($id);
        $file = public_path(). "/bukti_pembayaran/" .$pembayaran->bukti_pembayaran;
        return response()->download($file);
    }
}
