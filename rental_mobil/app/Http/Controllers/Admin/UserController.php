<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            return DataTables::of(User::select('id','name', 'email', 'role'))
                ->addIndexColumn()
                ->addColumn('action', 'admin.pages.user.action')
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.pages.user.index');
    }

    public function create()
    {
        return view('admin.pages.user.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required|in:admin,customer',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric',
            'no_ktp' => 'required|numeric',
            'user_id' => 'unique:profiles'
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);
        Profile::create([
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'no_ktp' => $request->no_ktp,
            'user_id' => $user->id
        ]);
       
        return redirect()->route('admin.user.index')->with('notif', 'Berhasil buat user');
    }

    public function show($id)
    {
        $data = User::with('profile')->find($id);
        return view('admin.pages.user.show',[
           'user' => $data
        ]);
    }

    public function edit($id)
    {
        $data = User::with('profile')->findOrFail($id);
        return view('admin.pages.user.edit',[
            'user' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'. $id,
            'password' => 'confirmed',
            'role' => 'required|in:admin,customer',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric',
            'no_ktp' => 'required|numeric',
        ]);
        $dataUser = $request->except(['alamat', 'no_telepon', 'no_ktp', '_token', '_method', 'password_confirmation']);
        $dataProfile = $request->only(['alamat', 'no_telepon', 'no_ktp']);
       
        if($request->password){
            $dataUser['password'] = Hash::make($request->password);
        } else {
            unset($dataUser['password']);
            unset($dataUser['password_confirmation']);
        }
        User::where('id', $id)->update($dataUser);
        Profile::where('user_id', $id)->update($dataProfile);
        return redirect()->route('admin.user.index')->with('notif', 'Berhasil update user');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('admin.user.index')->with('notif', 'User berhasil dihapus');
    }
}
