<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mobil;
use App\Pembayaran;
use App\Transaksi;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $total_users = User::count();
        $total_cars = Mobil::count();
        $total_transaksi = Transaksi::count();
        $total_pembayaran = Pembayaran::count();
        return view('admin.pages.dashboard', [
            't_users' => $total_users,
            't_cars' => $total_cars,
            't_transaksi' => $total_transaksi,
            't_pembayaran' => $total_pembayaran
        ]);
    }
}
