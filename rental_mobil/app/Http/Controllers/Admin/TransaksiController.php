<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transaksi;
use App\Pembayaran;

class TransaksiController extends Controller
{
    public function index()
    {
    	$key = 0;
        $transaksi = Transaksi::all();
    	return view('admin.pages.transaksi.index', compact('transaksi', 'key'));
    }

    public function show($id)
    {
        $transaksi = Transaksi::findOrfail($id);
        return view('admin.pages.transaksi.details', compact('transaksi'));
    }

    public function aksi($id)
    {
        $transaksi = Transaksi::findOrfail($id);
        return view('admin.pages.transaksi.action', compact('transaksi'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'status_pengembalian' => 'required',
            'status_transaksi' => 'required',
            'tanggal_kembali' => 'required',
            'tanggal_pengembalian' => 'required',
            'denda' => 'required',
        ]);

        $x = strtotime($request["tanggal_pengembalian"]);
        $y = strtotime($request["tanggal_kembali"]);
        $selisih = abs($x - $y)/(60*60*24);
        $total_denda = $selisih * $request["denda"]; 

        $query = DB::table('transaksi')
            ->where('id', $id)
            ->update([
                'status_pengembalian' => $request["status_pengembalian"],
                'status_transaksi' => $request["status_transaksi"],
                'tanggal_pengembalian' => $request["tanggal_pengembalian"],
                'total_denda' => $total_denda
            ]);
        return redirect()->route('admin.transaksi.index');
    }

    public function destroy($id)
    {
        $transaksi = Transaksi::findOrfail($id);
        $transaksi->delete();
        return redirect()->route('admin.transaksi.index');
    }
}
