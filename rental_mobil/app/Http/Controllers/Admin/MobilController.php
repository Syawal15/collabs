<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Mobil;
use App\Merk;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\File;
class MobilController extends Controller
{
    
    public function index(Request $request)
    {
       $mobil = Mobil::select('id','nama','no_plat', 'harga', 'gambar','merk_mobil_id')->with('merk');
        if($request->ajax()){
            return DataTables::of($mobil)
                ->addIndexColumn()
                ->addColumn('action', 'admin.pages.mobil.action')
                ->addColumn('gambar', function($mobil){
                    $url = asset('uploads/'. $mobil->gambar);
                    return '<img src="'.$url.'" border="0" width="100" class="img-rounded" align="center" />';
                })
                ->rawColumns(['action', 'gambar'])
                ->make(true);
        }
        return view('admin.pages.mobil.index');
    }

    public function create()
    {
        $merk = Merk::all();
        return view('admin.pages.mobil.create',compact('merk'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_plat' => 'required',
            'warna' => 'required',
            'tahun' => 'required|numeric',
            'harga' => 'required|numeric',
            'denda' => 'required|numeric',
            'gambar' => 'max:500|mimes:jpeg,jpg,png',
            'status' =>  'required|in:0,1',
            'merk_mobil_id' => 'required|numeric',
        ]);
        $gambar = null;
        if($request->hasFile('gambar')){
            $gambar = $request->file('gambar')->store('photo_mobil', 'public');
        }
        Mobil::create([
            'nama' => $request->nama,
            'no_plat' => $request->no_plat,
            'warna' => $request->warna,
            'tahun' => $request->tahun,
            'harga' => $request->harga,
            'denda' => $request->denda,
            'gambar' => $gambar,
            'status' =>  $request->status,
            'merk_mobil_id' => $request->merk_mobil_id,
        ]);
      
        return redirect()->route('admin.mobil.index')->with('notif', 'Berhasil tambah mobil');
    }

    public function show($id)
    {
        $mobil = DB::table('mobil')
        ->join('merk_mobil','mobil.merk_mobil_id','=','merk_mobil.id')
        ->where('mobil.id',$id)->first();
        return view('admin.pages.mobil.details',compact('mobil'));
    }

    public function edit($id)
    {
    
        $mobil = Mobil::where('id',$id)->first();
        $merk = Merk::all();
        return view('admin.pages.mobil.edit',compact('mobil','merk'));
    }

    public function update(Request $request, $id)
    {   
        $gambar = Mobil::select('gambar')->findOrFail($id);
       
        $request->validate([
            'nama' => 'required',
            'no_plat' => 'required',
            'warna' => 'required',
            'tahun' => 'required|numeric',
            'harga' => 'required|numeric',
            'denda' => 'required|numeric',
            'gambar' => 'max:500|mimes:jpeg,jpg,png',
            'status' =>  'required|in:0,1',
            'merk_mobil_id' => 'required|numeric',
        ]);
        $data = $request->except('_token', '_method');
        if($request->file('gambar')){
            File::delete(public_path('uploads/'. $gambar->gambar));
            $data['gambar'] = $request->file('gambar')->store('photo_mobil', 'public');
        }else{
            unset($data['gambar']);
        }
        Mobil::where('id',$id)->update($data);
        return redirect()->route('admin.mobil.index')->with('notif', 'Berhasil edit mobil');
    }

    public function destroy($id)
    {
        $mobil = Mobil::findOrFail($id);
        if($mobil->gambar){
            File::delete(public_path('uploads/'. $mobil->gambar));
        }
        $mobil->delete();
        return redirect()->route('admin.mobil.index')->with('notif', 'Berhasil hapus mobil');
    }
}
