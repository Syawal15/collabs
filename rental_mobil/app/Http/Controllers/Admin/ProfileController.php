<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Hash;
use Auth;
class ProfileController extends Controller
{
    public function index()
    {
        $data = User::with('profile')->find(Auth::user()->id);
        return view('admin.pages.profile.index', [
            'user' => $data
        ]);
    }
    public function update(Request $request){
        $id = Auth::user()->id;
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'. $id,
            'password' => 'confirmed',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric',
            'no_ktp' => 'required|numeric',
        ]);
        $dataUser = $request->except(['alamat', 'no_telepon', 'no_ktp', '_token', '_method', 'password_confirmation']);
        $dataProfile = $request->only(['alamat', 'no_telepon', 'no_ktp']);
        if($request->password){
            $dataUser['password'] = Hash::make($request->password);
        } else {
            unset($dataUser['password']);
            unset($dataUser['password_confirmation']);
        }
        User::where('id', $id)->update($dataUser);
        Profile::where('user_id', $id)->update($dataProfile);
        return redirect()->route('admin.profile.index')->with('notif', 'Berhasil update user');
    }
    
}
