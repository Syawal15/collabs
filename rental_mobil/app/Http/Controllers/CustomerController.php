<?php

namespace App\Http\Controllers;

use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class CustomerController extends Controller
{
    public function index(){
        return view('customer.pages.index');
    }

    public function mobil()
    {
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->get();
        return view('customer.pages.mobil', compact('mobil'));
    }

    public function details($id)
    {
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->where('mobil.id', '=', $id)
                ->get();
        return view('customer.pages.details', compact('mobil'));
    }

    public function rental($id)
    {
        $user = Auth::user()->id;
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->where('mobil.id', '=', $id)
                ->get();
        return view('customer.pages.rental', compact('mobil', 'user'));
    }

    public function transaksi($id, Request $request)
    {
        $request->validate([
            'tanggal_rental' => 'required',
            'tanggal_kembali' => 'required',
            'harga' => 'required',
            'denda' => 'required',
            'users_id' => 'required',
        ]);

        $x = strtotime($request["tanggal_kembali"]);
        $y = strtotime($request["tanggal_rental"]);
        $durasi_rental = abs($x - $y)/(60*60*24);
        $total_pembayaran = $request['harga'] * $durasi_rental;
        $status_pengembalian = 0;
        $status_transaksi = 0;
        $mobil_id = $id;

        $query = DB::table('transaksi')->insert([
            "tanggal_rental" => $request["tanggal_rental"],
            "tanggal_kembali" => $request["tanggal_kembali"],
            "harga" => $request["harga"],
            "denda" => $request["denda"],
            "users_id" => $request["users_id"],
            "mobil_id" => $mobil_id,
            "durasi_rental" => $durasi_rental,
            "total_pembayaran" => $total_pembayaran,
            "status_pengembalian" => $status_pengembalian,
            "status_transaksi" => $status_transaksi,
        ]);
 
        return redirect('/daftar');
    }

    public function daftar() 
    {
        $user = Auth::user()->id;
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->join('transaksi', 'mobil.id', '=', 'transaksi.mobil_id')
                ->where('transaksi.users_id', '=', $user)
                ->select('merk_mobil.nama', 'mobil.no_plat', 'transaksi.total_pembayaran', 'transaksi.status_transaksi', 'transaksi.status_pengembalian', 'transaksi.id')
                ->get();
        
        return view('customer.pages.daftar', compact('mobil', 'user'));
    }

    public function checkout($id) 
    {
        DB::table('transaksi')
        ->join('mobil', 'transaksi.mobil_id', '=', 'mobil.id')
        ->where('transaksi.id', '=', $id)
        ->update([
            'status_transaksi' => 1,
            'status' => 1,
            ]);

        DB::table('pembayaran')->insert([
            "status_pembayaran" => 0,
            "bukti_pembayaran" => '-',
            "transaksi_id" => $id,
        ]); 

        $user = Auth::user()->id;
        
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->join('transaksi', 'mobil.id', '=', 'transaksi.mobil_id')
                ->where('transaksi.users_id', '=', $user)
                ->select('merk_mobil.nama', 'mobil.no_plat', 'transaksi.total_pembayaran', 'transaksi.status_transaksi', 'transaksi.status_pengembalian', 'transaksi.id')
                ->get();
        
        return view('customer.pages.daftar', compact('mobil'));
    }

    public function pembayaran($id)
    {
        $user = Auth::user()->id;
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->join('transaksi', 'mobil.id', '=', 'transaksi.mobil_id')
                ->join('pembayaran', 'transaksi.id', '=', 'pembayaran.transaksi_id')
                ->where('transaksi.users_id', '=', $user)
                ->select('merk_mobil.nama', 'mobil.no_plat', 'transaksi.total_pembayaran', 'transaksi.status_transaksi', 'transaksi.status_pengembalian', 'transaksi.id', 'transaksi.tanggal_rental', 'transaksi.tanggal_kembali', 'transaksi.harga', 'transaksi.durasi_rental', 'transaksi.total_pembayaran', 'pembayaran.status_pembayaran')
                ->get();
        
        return view('customer.pages.pembayaran', compact('mobil'));
    }

    public function upload($id, Request $request) {
        
        $request->validate([
			'bukti_pembayaran' => 'required',
		]);

        $file = $request->file('bukti_pembayaran');

        $name = 'Bukti_Pembayaran_' . $id. '.jpg'; 

        $tujuan_upload = public_path(). "/bukti_pembayaran";

        $file->move($tujuan_upload,$name);

        DB::table('pembayaran')
        ->where('transaksi_id', '=', $id)
        ->update([
            'bukti_pembayaran' => $name,
            'status_pembayaran' => 1,
            ]);
            
        $user = Auth::user()->id;
        $mobil = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->join('transaksi', 'mobil.id', '=', 'transaksi.mobil_id')
                ->where('transaksi.users_id', '=', $user)
                ->select('merk_mobil.nama', 'mobil.no_plat', 'transaksi.total_pembayaran', 'transaksi.status_transaksi', 'transaksi.status_pengembalian', 'transaksi.id')
                ->get();
        
        return view('customer.pages.daftar', compact('user', 'mobil'));
    }

    public function print($id)
    {
        $user = Auth::user()->id;
        $transaksi = DB::table('mobil')
                ->join('merk_mobil', 'mobil.merk_mobil_id', '=', 'merk_mobil.id')
                ->join('transaksi', 'mobil.id', '=', 'transaksi.mobil_id')
                ->join('pembayaran', 'transaksi.id', '=', 'pembayaran.transaksi_id')
                ->where('transaksi.users_id', '=', $user)
                ->select('merk_mobil.nama', 'mobil.no_plat', 'transaksi.total_pembayaran', 'transaksi.status_transaksi', 'transaksi.status_pengembalian', 'transaksi.id', 'transaksi.tanggal_rental', 'transaksi.tanggal_kembali', 'transaksi.harga', 'transaksi.durasi_rental', 'transaksi.total_pembayaran', 'pembayaran.status_pembayaran')
                ->get();
        
        return view('customer.pages.cetak_invoice', compact('user', 'transaksi'));
    }
}
