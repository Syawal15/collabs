<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Transaksi;
Use App\User;
Use App\Profile;
use Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index()
    {
        return view('customer.pages.dashboard.index', [
            't_transaksi' => Transaksi::where('users_id', Auth::user()->id)->count()
        ]);
    }
    public function profile()
    {
        $data = User::with('profile')->find(Auth::user()->id);
        return view('customer.pages.dashboard.profile', [
            'user' => $data
        ]);
    }
    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'. $id,
            'password' => 'confirmed',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric',
            'no_ktp' => 'required|numeric',
        ]);
        $dataUser = $request->except(['role', 'alamat', 'no_telepon', 'no_ktp', '_token', '_method', 'password_confirmation']);
        $dataProfile = $request->only(['alamat', 'no_telepon', 'no_ktp']);
        if($request->password){
            $dataUser['password'] = Hash::make($request->password);
        } else {
            unset($dataUser['password']);
            unset($dataUser['password_confirmation']);
        }
        User::where('id', $id)->update($dataUser);
        Profile::where('user_id', $id)->update($dataProfile);
        return redirect()->route('customer.profile')->with('notif', 'Berhasil update user');
    }
    public function transaksi()
    {
        return view('customer.pages.dashboard.transaksi');
    }
}
