<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    protected $fillable = ['tanggal_pengembalian', 'status_pengembalian', 'status_transaksi', 'total_denda'];

    public $timestamps = false;

    public function pembayaran()
    {
        return $this->hasMany('App\Pembayaran');
    }
}
