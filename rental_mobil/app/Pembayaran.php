<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';

    protected $fillable = ['status_pembayaran'];

    public $timestamps = false;

    public function transaksi() 
    {
        return $this->belongsTo('App\Trasansaksi');
    }
}
