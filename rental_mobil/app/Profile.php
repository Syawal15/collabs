<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['alamat', 'no_telepon', 'no_ktp', 'user_id'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
