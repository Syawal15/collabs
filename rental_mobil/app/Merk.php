<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    protected $table = 'merk_mobil';
    public $timestamps = false;

    public function cars()
    {
        return $this->hasMany('App\Mobil');
    }
}
