<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = "mobil";
    protected $fillable = ["nama","no_plat","warna","tahun","harga","denda","gambar","status","merk_mobil_id"];
    public $timestamps = false;

    public function merk()
    {
        return $this->belongsTo('App\Merk', 'merk_mobil_id');
    }
}
