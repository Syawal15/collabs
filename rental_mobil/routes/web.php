<?php

Route::name('customer.')->group(function(){
    Route::get('/', 'CustomerController@index')->name('index');
    Route::get('/mobil', 'CustomerController@mobil')->name('mobil');
    Route::get('/mobil/{mobil}', 'CustomerController@details')->name('customer.details');
    
    Route::middleware(['auth'])->group(function(){
        Route::get('/rental/{mobil}', 'CustomerController@rental')->name('customer.rental');
        Route::post('/transaksi/{mobil}', 'CustomerController@transaksi')->name('customer.transaksi');
        Route::get('/daftar', 'CustomerController@daftar')->name('customer.daftar');
        Route::get('/checkout/{transaksi}', 'CustomerController@checkout')->name('customer.checkout');
        Route::get('/pembayaran/{transaksi}', 'CustomerController@pembayaran')->name('customer.pembayaran');
        Route::post('/upload/{transaksi}', 'CustomerController@upload')->name('customer.upload');
        Route::get('/print/{transaksi}', 'CustomerController@print')->name('customer.print');
        
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/dashboard/profile', 'DashboardController@profile')->name('profile');
        Route::put('/dashboard', 'DashboardController@update')->name('update');
        Route::get('/dashboard/transaksi', 'DashboardController@transaksi')->name('transaksi');
    });
});

Auth::routes();

Route::middleware(['web','auth', 'admin'])
    ->namespace('Admin')
    ->prefix('admin')
    ->name('admin.')
    ->group(function(){
        Route::get('/', 'DashboardController@index')->name('dashboard.index');
        
        Route::resource('user', 'UserController');
        Route::get('/profile', 'ProfileController@index')->name('profile.index');
        Route::put('/profile/update', 'ProfileController@update')->name('profile.update');

        Route::resource('mobil','MobilController');
        Route::resource('merk','MerkController');
        Route::get('/mobil/{{$value->id}}','MobilController@show');
        Route::get('/mobil/{{$value->id}}/edit','MobilController@edit');
        Route::put('/mobil/{{$value->id}}','MobilController@update');
        Route::post('/merklist','MerKController@storemerk')->name('merk.storemerk');
        Route::delete('/mobil/{{$value->id}}','MobilController@destroy');
        Route::delete('/merk/{{$value->id}}','MobilController@destroy');

        Route::get('/transaksi/{transaksi}/aksi', 'TransaksiController@aksi')->name('transaksi.aksi');
        Route::resource('/transaksi', 'TransaksiController');
      
        Route::get('/pembayaran/{transaksi}', 'PembayaranController@show')->name('pembayaran.show');
        Route::put('/pembayaran/{transaksi}', 'PembayaranController@update')->name('pembayaran.update');
        Route::get('/pembayaran/{transaksi}/download', 'PembayaranController@download')->name('pembayaran.download');

    });

