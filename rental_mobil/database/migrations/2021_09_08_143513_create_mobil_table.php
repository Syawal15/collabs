<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('no_plat');
            $table->string('warna');
            $table->string('tahun');
            $table->integer('harga');
            $table->integer('denda');
            $table->string('gambar')->nullable();
            $table->string('status');
            $table->unsignedBigInteger('merk_mobil_id');
            $table->foreign('merk_mobil_id')
                    ->nullable()
                    ->references('id')
                    ->on('merk_mobil')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobil');
    }
}
