<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal_rental');
            $table->date('tanggal_kembali');
            $table->integer('durasi_rental');
            $table->integer('harga');
            $table->integer('denda');
            $table->integer('total_pembayaran');
            $table->date('tanggal_pengembalian')->nullable();
            $table->integer('total_denda')->nullable();
            $table->integer('status_pengembalian');
            $table->integer('status_transaksi');
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('mobil_id');
            $table->foreign('users_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('mobil_id')
                    ->references('id')
                    ->on('mobil')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
